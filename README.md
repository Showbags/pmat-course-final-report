# PMAT Course Final

This is a report written for the PMAT Course Final:

- <https://academy.tcm-sec.com/p/practical-malware-analysis-triage>

Objectives of the course final are a followS:

    Choose any single sample from this course.
    Write a triage report for your chosen sample.
    Write a set of Yara rules for the sample.
    Then, publish the report and rules open-source and tell the world.

I have chosen to review the malware sample `RAT.Unknown.exe.malz`

Please refer to the `PMAT-report-1.0.pdf` for the triage report.

The Yara rules for the sample are contained within the data folder.

Report was writtin in markdown and compiled with Report Ranger:

- <https://gitlab.com/volkis/report-ranger>

I will upload the template that I created for Report Ranger so others can compile a PMAT report
if they so wish.
