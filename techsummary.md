<!-- DemoWare consists of two parts: an encrypted stage 0 dropper and an unpacked and decoded stage 2 command execution program. It first attempts to contact its callback URL (hxxps://demowarecallback.local) and unpacks its stage 2 payload if successful -->

<!-- include diagrams -->

## High-Level Technical Summary

- {{mname}} is an x64 Windows executable.
- {{mname}} is Nim-compiled:
  - https://nim-lang.org/
- {{mname}} consists of a single payload that functions in two stages:

### Stage 1 {-}

1. {{mname}} attempts a HTTP request to the URL:

   - `serv1.ec2-102-95-13-2-ubuntu.local`

2. **IF** `serv1.ec2-102-95-13-2-ubuntu.local` is reachable {{mname}} moves to **Stage 2** **ELSE** {{mname}} terminates.

![{{mname}} Stage 1 behavior](./screenshots/RAT_high_level_stage_1_diagram_updated.png)

### Stage 2 {-}

1. {{mname}} creates the folder `XEQ0CBZ1` in users `%localappdata%` IE cache:

    - `%localappdata%\Microsoft\Windows\INetCache\IE\XEQ0CBZ1\`

2. {{mname}} generates a `.htm` file with a randomized name in the IE cache folder `XEQ0CBZ1`.
3. {{mname}} attempts a HTTP GET request for the following file:
   
    - `http://serv1.ec2-102-95-13-2-ubuntu.local/msdcorelib.exe`

4. {{mname}} downloads the `msdcorelib.exe` file to the users `%appdata%` Startup folder:

    - `%appdata%\Microsoft\Windows\Start Menu\Programs\Startup\mscordll.exe`

**Note**: 

    - The file name download `msdcorelib.exe` changes its name to `mscordll.exe` when it is saved to the users startup directory. 
    - Placing a file in a users startup directory is common persistence behavior. `msdcorlib.exe` will run automatically when a user logs in.

5. {{mname}} opens a bind shell on TCP port 5555 allowing for remote command execution.

![{{mname}} Stage 2 behaviour](./screenshots/RAT_high_level_stage_2_diagram.png)