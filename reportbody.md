---
title: RAT.Unknown.exe.malz
client: PMAT Course Final
template: sample
author: Joshua Rynan
authoremail: josh@volkis.com.au
authormobile: "-"
vulndir: "vulnerabilities"
appendixdir: "appendices"
changes:
- [0.1, 2023-01-28, "Joshua Rynan", "Initial report"]
- [1.0, 2023-01-28, "Joshua Rynan", "Initial release"]
contributors:
- ["Joshua Rynan", "Senior Security Consultant", "NA", "josh@volkis.com.au"]
mname: RAT.Unknown.exe.malz
---

{{ of.newsection() }}

# Executive summary {-}

{{ include_file('executivesummary.md') }}

{{ new_section() }}

# Technical summary {-}

{{ include_file('techsummary.md') }}

{{ new_section() }}

# Malware analysis {-}

{{ include_file('malwareanalysis.md') }}