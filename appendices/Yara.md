---
name: Yara Rules
weight: 1
...

\label{appendix:yara}

## Rules and signatures {-}

The following YARA rule will help detect the {{mname}} application.

```
rule Yara_Example {
    
    meta: 
        last_updated = "2023-02-23"
        author = "Showbags"
        description = "YARA rule for detecting RAT.Unknown.exe.malz"

    strings:
        // Fill out identifying strings and other criteria
        $string1 = "[+] what command can I run for you" ascii
        $string2 = "nim" ascii
        $urlString = "http://serv1.ec2-102-95-13-2-ubuntu.local" ascii
        $malwareError = "NO SOUP FOR YOU" ascii
        $persistenceExeDownload = "msdcorelib.exe" ascii
        $persistenceExe = "mscorlib.exe" ascii
        $persistenceFolder = "AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup" ascii
        $PE_magic_byte = "MZ"
        $startToDownload_hex_string = { E8 C9 F7 FF FF }
        $startServer_hex_string = { E8 EF F9 FF FF }

    condition:
        // Fill out the conditions that must be met to identify the binary
        $PE_magic_byte at 0 and
        ($string1 and $string2) or
        ($urlString and $malwareError) or
        ($persistenceExeDownload and $persistenceExe and $persistenceFolder) or

        $startToDownload_hex_string or

        $startServer_hex_string

}
```