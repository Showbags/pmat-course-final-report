---
name: Document control
weight: -100
...

### Document information {-}

{{
  of.table([
    ["Client", client],
    ["Document name", title],
    ["Document version", version]
  ], colwidths=[0.35, 0.65], headings='left')
}}

### Document changes {-}

{{ of.table(changes, header=['Version', 'Date', 'Name', 'Changes'], headings='top', colalign=[
                                               'l', 'l', 'l', 'l'], colwidths=[13, 17, 30, 55]) }}

{{ of.table(contributors, header=['Name', 'Role', 'Phone number', 'Email address'], colwidths=[0.22, 0.25, 0.2, 0.33], headings='top')}}