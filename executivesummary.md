<!-- DemoWare is a cryptor-dropper malware sample first identified on Oct 15th, 2021. It is a GoLang-compiled dropper that runs on the x64 Windows operating system. It consists of two payloads that are executed in succession following a successful spearphishing attempt. Symptoms of infection include infrequent beaconing to any of the URLs listed in Appendix B, random blue screen popups on the endpoint, and an executable named “srvupdate.exe” appearing in the %APPDATA% directory.

YARA signature rules are attached in Appendix A. Malware sample and hashes have been submitted to VirusTotal for further examination.
-->

<!--

Include a glossary or links to relevant terms etc.

-->

{{ ft(colwidths=[2,8]) }}

| Algorithm | Hash                                                                |
| :------   | :-------                                                            |
| md5       | 689FF2C6F94E31ABBA1DDEBF68BE810E                                    |
| sha1      | 69B8ECF6B7CDE185DAED76D66100B6A31FD1A668                            |
| sha256    | 248D491F89A10EC3289EC4CA448B19384464329C442BAC39 5F680C4F3A345C8C   |

{{mname}} is a Remote Access Tojan (RAT) [^RAT] sample first identified on January 21st, 2023.

A RAT is a type of malicious software that allows an attacker to take control of a computer from a remote location, enabling them to steal sensitive information, install additional malware, and conduct other malicious activities.

{{mname}} is Nim-compiled RAT that runs on the x64 Windows operating system. It consists of a single payload that functions in two stages:

- Stage 1: Checks to see if there is an Internet connection, terminating the application if a pre defined URL (Listed in **\nameref{appendix:Callback URL's}**) is inaccessible.
- Stage 2: If the pre defined URL is accessible the application will perform the following actions:
  - Write unknown `.htm` file to temporary Internet Explorer (IE) cache.
  - Write the unknown executable to User's startup folder, in-line with known persistence techniques.
  - Serve a bind shell [^bind_shell] on TCP port 5555 giving command line access to an attacker.

![{{mname}} process flow](./screenshots/RAT_exec_diagram.png)

YARA signature rules are attached in **\nameref{appendix:yara}**For a more detailed analysis of the {{mname}} executable please refer to **\nameref{malwarecomp:Malware Composition}**.

Malware sample and hashes have been submitted to VirusTotal for further examination.

[^RAT]: <https://en.wikipedia.org/wiki/Remote_desktop_software#RAT>
[^bind_shell]: <https://www.geeksforgeeks.org/difference-between-bind-shell-and-reverse-shell/>